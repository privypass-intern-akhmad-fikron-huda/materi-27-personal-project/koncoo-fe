import { createRouter, createWebHistory } from 'vue-router'
// import HomeView from '../views/HomeView.vue'
import MemberView from '../views/MemberView.vue'
import LoginView from '../views/LoginView.vue'
import HomeView from '../views/HomeView.vue'
import RegistrationView from '../views/RegistrationView.vue'
import DetailMemberView from '../views/DetailMemberView.vue'
import EditMemberView from '../views/EditMemberView.vue'
import AddCircleView from '../views/AddCircleView.vue'
import AddMemberView from '../views/AddMemberView.vue'
import { useUserStore } from "@/stores/user"

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView,
      meta: { requireAuth: true },
      children: [
        {
          path: "members",
          name: 'members',
          component: MemberView,
          meta: { requireAuth: true },
        },
        {
          path: "members/add",
          name: 'addmembers',
          component: AddMemberView,
          meta: { requireAuth: true },
        },
        {
          path: "detail/:id",
          name: 'detail', 
          component: DetailMemberView,
          meta: { requireAuth: true },
        },
        {
          path: "detail/:id/edit",
          name: 'editmember', 
          component: EditMemberView,
          meta: { requireAuth: true },
        },
        {
          path: "circle/add",
          name: 'addcircel', 
          component: AddCircleView,
          meta: { requireAuth: true },
        }
      ]
    },
    {
      path: '/login',
      name: 'login',
      component: LoginView
    },
    {
      path: '/registration',
      name: 'registration',
      component: RegistrationView
    },
    /* {
      path: '/',
      name: 'home',
      component: MemberView
    }, */
  ]
})

router.beforeEach((to, from) => {
  const { user } = useUserStore();

  if (to.meta.requireAuth && !user) {
    return {
      path: "/login"
    };
  }

  if (user && (to.name === "login" || to.name === "register")) {
    return { name: "home" };
  }
});

export default router
